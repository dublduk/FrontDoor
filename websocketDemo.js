// Establish the WebSocket connection and set up event handlers
let ws = new WebSocket("ws://" + location.hostname + ":" + location.port + "/chat");
ws.onmessage = msg => updateChat(msg);
ws.onclose = () => alert("WebSocket connection closed");

// Add event listeners to button and input field
document.getElementById("send").addEventListener("click", () => sendAndClear(document.getElementById("message").value));
document.getElementById("message").addEventListener("keypress", function (e) {
    if (e.keyCode === 13) { // Send message if enter is pressed in input field
        sendAndClear(e.target.value);
    }
});

function sendAndClear(message) {
    if (message !== "") {
        ws.send(message);
        document.getElementById("message").value = "";
    }
}

function updateChat(msg) { // Update chat-panel and list of connected users
    let data = JSON.parse(msg.data);
    let message = "<article><b>" + data.userMessage.sender + " says:</b><span class=\"timestamp\">" + currentTime() + "</span>"
        + "<p>" + data.userMessage.message + "</p></article>";
    document.getElementById("chat").insertAdjacentHTML("afterbegin", message);
    document.getElementById("userList").innerHTML = data.userList.map(user => "<li>" + user.username + "</li>").join("");
}

function currentTime() {
    let now = new Date();
    return now.toLocaleTimeString();
}
